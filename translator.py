
class Translator(object):
    def __init__(self):
        self.asmoChar = []
        self.asmoNextTable = []
        self.elyoChar = []
        self.elyoNextTable = []
        #self.transMode = 0 # 0=asmo 1=elyos

        self.asmoChar.append(['','j','k','h','i','n','o','l','m','r','s','p','q','v','w','t','u','z','G','b','c','J','a','f','g','d','e'])
        self.asmoChar.append(['','e','f','c','d','i','j','g','h','m','n','k','l','q','r','o','p','u','v','s','t','y','z','a','b','I','J'])
        self.asmoChar.append(['','f','g','d','e','j','k','h','i','n','o','l','m','r','s','p','q','v','w','t','u','z','G','b','c','J','a'])
        self.asmoChar.append(['','g','h','e','f','k','l','i','j','o','p','m','n','s','t','q','r','w','x','u','v','G','H','c','d','a','b'])

        self.elyoChar.append(['','i','h','k','j','m','l','o','n','q','p','s','r','u','t','w','v','y','x','a','z','c','b','e','d','g','f'])
        self.elyoChar.append(['','d','c','f','e','h','g','j','i','l','k','n','m','p','o','r','q','t','s','v','u','x','w','z','y','b','a'])
        self.elyoChar.append(['','e','d','g','f','i','h','k','j','m','l','o','n','q','p','s','r','u','t','w','v','y','x','a','z','c','b'])

        self.asmoNextTable.append([0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,2,2,3,2,2,2,2,2]);
        self.asmoNextTable.append([0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2,3,3]);
        self.asmoNextTable.append([0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,2,2,3,2]);
        self.asmoNextTable.append([0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,3,3,2,2,2,2]);

        self.elyoNextTable.append([0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,2,2,2,2,2,2]);
        self.elyoNextTable.append([0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,2]);
        self.elyoNextTable.append([0,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,2,1,2,2]);

    def letterIndex(self, letter):
        if ord(letter) in range(ord('a'), ord('z')+1):
            return ord(letter) - 96
        else:
            return 0

    def translate(self, input, transMode=0):
        output = ''
        tableIndex = 0

        # slice string if more than 195 long
        if len(input) > 195:
            input = input[:195]
            
        # lower case all
        input = input.lower()

        if(transMode == 0):
            fracChar = self.asmoChar
            fracNextTable = self.asmoNextTable
        else:
            fracChar = self.elyoChar
            fracNextTable = self.elyoNextTable
        
        for i, char in enumerate(input):
            if self.letterIndex(char) != 0:
                output += fracChar[tableIndex][self.letterIndex(char)];
                tableIndex = fracNextTable[tableIndex][self.letterIndex(char)];
            else:
                output += char
                tableIndex = 0;
        return output
    
    def elyo_to_asmo(self, txt):
        return self.translate(txt, 1)
        
    def asmo_to_elyo(self, txt):
        return self.translate(txt, 0)
    
if __name__ == '__main__':
    trans = Translator()
    print trans.elyo_to_asmo('testowy tekst')
    print trans.asmo_to_elyo('testowy tekst')
